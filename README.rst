vup
===
Version up your sources. This is a rewrite of the awesome
bumpverison_.
It was written partly as a learning excercise and partly so I
could have a tool like bumpversion without the need for a
runtime.

.. _bumpversion: https://github.com/peritus/bumpversion

